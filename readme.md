# gBem
gBem is command line tool that help creation BEM structure, like this:

```
myblock
├── myblock.bemtree.js
├── myblock.css
└── __elem
    ├── myblock__elem.bemtree.js
    └── myblock__elem.css
```

node version must be >=6.8.1

## Install
```bash
npm install -g https://bblc@bitbucket.org/bblc/gbem.git
```
## Usage
see help
```bash
gbem --help
```

create new level
```bash
gbem -b bundleName
```

create new block on new level
```bash
gbem blockName -b bundleName
```
command create block-name.bemdecl.js file

create new block
```bash
gbem blockName
```

create new block element
```bash
gbem blockName__blockElem
```

create new block modificator
```bash
gbem blockName_blockMod
gbem blockName_blockMod_blockModVal
```


```bash
gbem block-name
```
command create block-name.bemtree.js file

for creation another filetype pass tech in -t flag
```bash
gbem block-name -t bemhtml.js
```
creates block-name.bemhtml.js file

to create several filetypes pass techs in -t flag with ',' separator
```bash
gbem block-name -t bemhtml.js,styl,js
```
creates block-name.bemhtml.js, block-name.styl, block-name.js files

list of supported tech
bemdecl.js, bemhtml.js, bemtree.js, deps.js, styl, css
