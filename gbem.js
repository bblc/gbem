#!/usr/bin/env node

'use strict';

const fs = require('fs');
const path = require('path');
const program = require('commander');
const mkpath = require('mkpath');
const Mustache = require('mustache');
const l = require('lodash');


const get_settings = () => {
    const defaultSettings = JSON.parse(fs.readFileSync(path.join(__dirname, 'gbem.json'), "utf8"));
    let userSettings = {};
    if (fs.existsSync('gbem.json')) {
        userSettings = JSON.parse(fs.readFileSync('gbem.json', "utf8"));
    }

    const finalSettings = l.assign(defaultSettings, userSettings);

    return finalSettings;
}

const getBemObj = (fileName) => {
    const elemDelim = get_settings().delim.elem;
    const modDelim = get_settings().delim.mod;
    const arr = fileName.split(elemDelim);
    let obj = {};
    let elemArr, blockArr;

    if (arr.length > 2) {
        return "ERROR! You can't create element of element";
    } else if (arr.length == 2) {
        obj.blockName = arr[0];
        elemArr = arr[1].split(modDelim);
        if (elemArr.length > 3) {
            return "Some error in naming.";
        } else if (elemArr.length == 3) {
            obj.elemName = elemArr[0];
            obj.elemMod = elemArr[1];
            obj.elemModVal = elemArr[2];
        } else if (elemArr.length == 2) {
            obj.elemName = elemArr[0];
            obj.elemMod = elemArr[1];
        } else if (elemArr.length == 1) {
            obj.elemName = elemArr[0];
        } else {
            return "some strange error";
        }
    } else if (arr.length == 1) {
        blockArr = arr[0].split(modDelim);
        if (blockArr.length > 3) {
            return "Some error in naming.";
        } else if (blockArr.length == 3) {
            obj.blockName = blockArr[0];
            obj.blockMod = blockArr[1];
            obj.blockModVal = blockArr[2];
        } else if (blockArr.length == 2) {
            obj.blockName = blockArr[0];
            obj.blockMod = blockArr[1];
        } else if (blockArr.length == 1) {
            obj.blockName = blockArr[0];
        } else {
            return "some strange error";
        }
    }

    return obj;
}

const isBlock = (fileName) => {
    return l.size(getBemObj(fileName)) == 1;
}

const isElem = (fileName) => {
    return l.has(getBemObj(fileName), 'elemName') && !l.has(getBemObj(fileName), 'elemMod');
}

const isBlockMod = (fileName) => {
    return l.has(getBemObj(fileName), 'blockMod');
}

const isElemMod = (fileName) => {
    return l.has(getBemObj(fileName), 'elemMod');
}

const hasBlockModValue = (fileName) => {
    return l.has(getBemObj(fileName), 'blockModVal');
}

const hasElemModValue = (fileName) => {
    return l.has(getBemObj(fileName), 'elemModVal');
}

const getBlockName = (fileName) => {
    return getBemObj(fileName).blockName || '';
}

const getBlockModName = (fileName) => {
    return getBemObj(fileName).blockMod || '';
}

const getBlockModVal = (fileName) => {
    return getBemObj(fileName).blockModVal || '';
}

const getElemName = (fileName) => {
    return getBemObj(fileName).elemName || '';
}

const getElemModName = (fileName) => {
    return getBemObj(fileName).elemMod || '';
}

const getElemModVal = (fileName) => {
    return getBemObj(fileName).elemModVal || '';
}


const writeToFile = (filePath, str) => {
    mkpath(path.dirname(filePath), function(){
        fs.writeFileSync(filePath, str, 'utf8');
    });
}


function genFileProps(file, bundle, ext) {
    const settings = get_settings();

    let filePath, tmpl, tmplType, blockName, elemName, modName, modValName, startPath,
        tmplObj={
            'delimElem': settings.delim.elem,
            'delimMod': settings.delim.mod
            };



    if (bundle === true) {
        filePath = path.join(
                    settings.bundlesPath,
                    file,
                    file + '.bemdecl.js'
                );
        tmpl = fs.readFileSync(path.join(__dirname, 'tmpl/bemdecl.mustache'), "utf8");
    } else {
        blockName = getBlockName(file);
        tmplObj.blockName = blockName;

        if (bundle && bundle.length > 0) {
            startPath = path.join(settings.bundlesPath, bundle, 'blocks', blockName);
        } else {
            startPath = path.join(settings.blocksPath, blockName);

        }

        if (isBlock(file)) {
            filePath = path.join(
                        startPath,
                        blockName + '.' + ext
                    );
        } else if (isElem(file)) {
            elemName = getElemName(file);
            tmplObj.elemName = elemName;
            filePath = path.join(
                        startPath,
                        settings.delim.elem + elemName,
                        blockName + settings.delim.elem + elemName + '.' + ext
                    );
        } else if (isBlockMod(file)) {
            modName = getBlockModName(file);
            tmplObj.modName = modName;
            if (hasBlockModValue(file)) {
                modValName = getBlockModVal(file);
                tmplObj.modVal = modValName;
                filePath = path.join(
                            startPath,
                            settings.delim.mod + modName,
                            blockName + settings.delim.mod + modName + settings.delim.mod + modValName + '.' + ext
                        );
            } else {
                filePath = path.join(
                            startPath,
                            settings.delim.mod + modName,
                            blockName + settings.delim.mod + modName + '.' + ext
                        );
            }
        } else if (isElemMod(file)) {
            elemName = getElemName(file);
            modName = getElemModName(file);
            tmplObj.elemName = elemName;
            tmplObj.modName = modName;
            if (hasElemModValue(file)) {
                modValName = getElemModVal(file);
                tmplObj.modVal = modValName;
                filePath = path.join(
                            startPath,
                            settings.delim.elem + elemName,
                            settings.delim.mod + modName,
                            blockName + settings.delim.elem + elemName + settings.delim.mod + modName + settings.delim.mod + modValName + '.' + ext
                        );
            } else {
                filePath = path.join(
                            startPath,
                            settings.delim.elem + elemName,
                            settings.delim.mod + modName,
                            blockName + settings.delim.elem + elemName + settings.delim.mod + modName + '.' + ext
                        );
            }
        } else {
            return "some weird ERROR"
        }

        tmplType = ext.split('.')[0];
        try {
            tmpl = fs.readFileSync(path.join(__dirname, 'tmpl', tmplType +'.mustache'), "utf8");
        } catch (err) {
            console.log("No such template or some other error. Created empty template!");
            tmpl = fs.readFileSync(path.join(__dirname, 'tmpl', 'empty.mustache'), "utf8");
        }
    }

    return {
        tmpl: tmpl,
        tmplObj: tmplObj,
        filePath: filePath
    }
}

function createFile(file, bundle, ext='bemtree.js') {
    const {tmpl, tmplObj, filePath} = genFileProps(file, bundle, ext);

    const str = Mustache.render(tmpl, tmplObj);

    writeToFile(filePath, str);
}




function list(val) {
  return val.split(',');
}

program
    .arguments('<file>')
    .option('-t, --techs <techs>', 'The technology suffix that would be created', list)
    .option('-b, --bundle [bundle]', 'The bundle/page name')
    .action(function(file) {

        if (program.techs) {
            for (const ext of program.techs) {
                createFile(file, program.bundle, ext);
            }
        } else {
            createFile(file, program.bundle)
        }



    })
    .parse(process.argv);
